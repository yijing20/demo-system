package com.example.demo.repository;

import com.example.demo.entity.EntityUserCreate;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository
@ComponentScan("com.example.demo")
public interface UserRepo extends JpaRepository<EntityUserCreate, Integer> {
    List<EntityUserCreate> findAllByUserID(Integer id);
}
