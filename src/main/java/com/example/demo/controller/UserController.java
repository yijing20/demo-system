package com.example.demo.controller;


import com.example.demo.entity.EntityUserCreate;
import com.example.demo.service.UserService;
import com.example.demo.service.UserServiceImpl;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Controller
@RequestMapping("/demo")
@SecurityRequirement(name = "sas_api")
public class UserController {

    @Autowired
    private UserServiceImpl userService;

    @Operation(description = "Create new user",responses = {
            @ApiResponse(responseCode  = "1", description  = ""),
            @ApiResponse(responseCode  = "200", description  = ""),
            @ApiResponse(responseCode  = "401", description  = "")} )
    @PostMapping("/create-user")
    public ResponseEntity<String> createUser(@RequestBody EntityUserCreate entityUserCreate){
//        ResponseEntity<String> re = null;
        try{
            userService.saveUser(entityUserCreate);
            return new ResponseEntity<>("User created successfully", HttpStatus.OK);
        }catch(Exception e){
            return new ResponseEntity<String>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }



}
