package com.example.demo.constant;

import com.example.demo.repository.UserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ApplicationServiceImpl {
    @Autowired
    public PropertiesServiceImpl propertiesService;

    @Autowired
    public UserRepo userRepo;
}
