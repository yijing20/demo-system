package com.example.demo.constant;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class PropertiesServiceImpl {

    @Value(value = "${broadcast.swagger.version}")
    public String swaggerVersion;
    @Value(value = "${broadcast.swagger.project-title}")
    public String swaggerProjectTitle;
    @Value(value = "${broadcast.swagger.project-description}")
    public String swaggerProjectDescription;
    @Value(value = "${broadcast.swagger.contact-name}")
    public String swaggerContactName;
    @Value(value = "${broadcast.swagger.contact-website}")
    public String swaggerContactWebsite;
    @Value(value = "${broadcast.swagger.contact-email}")
    public String swaggerContactEmail;
}
