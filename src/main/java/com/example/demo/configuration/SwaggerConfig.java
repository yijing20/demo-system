package com.example.demo.configuration;

import io.swagger.v3.oas.annotations.enums.SecuritySchemeIn;
import io.swagger.v3.oas.annotations.enums.SecuritySchemeType;
import io.swagger.v3.oas.annotations.security.SecurityScheme;
import io.swagger.v3.oas.models.OpenAPI;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import io.swagger.v3.oas.models.info.Info;
import org.springframework.context.annotation.Primary;

//@Configuration
//@SecurityScheme(name = "demo_api", scheme = "bearer", type = SecuritySchemeType.HTTP, in = SecuritySchemeIn.HEADER,bearerFormat="JWT")
//public class SwaggerConfig {
//    @Bean
//    @Primary
//    public OpenAPI config() {
//        return new OpenAPI()
//                .info(new Info().title("Demo System API")
//                        .description("Micro-Service Documentation")
//                        .version("v2.0.0"));
//
//    }
//
//}