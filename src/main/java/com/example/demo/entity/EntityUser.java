package com.example.demo.entity;


import com.fasterxml.jackson.annotation.JsonIgnore;
import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

@MappedSuperclass
public abstract class EntityUser {
    @JsonIgnore
    @Column(name ="create_date", nullable = false)
    private Long createDate;

    @JsonIgnore
    @Column(name = "update_date", nullable = true)
    private Long updateDate;

    @JsonIgnore
    @Column(name = "delete_date", nullable = true)
    private Long deleteDate;

    @JsonIgnore
    @Column(name = "is_deleted", nullable = false)
    private Boolean deleted = false;

    public Long getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Long createDate) {
        this.createDate = createDate;
    }

    public Long getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Long updateDate) {
        this.updateDate = updateDate;
    }

    public Long getDeleteDate() {
        return deleteDate;
    }

    public void setDeleteDate(Long deleteDate) {
        this.deleteDate = deleteDate;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }
}
