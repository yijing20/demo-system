package com.example.demo.service;

import com.example.demo.entity.EntityUserCreate;
import com.example.demo.repository.UserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.example.demo.service.UserService;

import java.util.List;

@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserRepo userRepo;

    @Override
    public List<EntityUserCreate> getAllUser() {
        return userRepo.findAll();
    }

    @Override
    @Transactional
    public Long saveUser(EntityUserCreate entityUserCreate){
        return userRepo.save(entityUserCreate).getId();
    }

    @Override
    @Transactional
    public void updateUser(EntityUserCreate entityUserCreate){
        userRepo.save(entityUserCreate);
    }

    @Override
    @Transactional
    public void deleteUser(int id){
        userRepo.deleteById(id);
    }

    @Override
    @Transactional
    public EntityUserCreate getUser(Integer id){
        return userRepo.findById(id).orElse(null);
    }




}
