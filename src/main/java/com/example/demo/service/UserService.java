package com.example.demo.service;

import com.example.demo.entity.EntityUserCreate;

import java.util.List;

public interface UserService {

    public List<EntityUserCreate> getAllUser();

    public Long saveUser(EntityUserCreate entityUserCreate);

    public void updateUser(EntityUserCreate entityUserCreate);

    public void deleteUser(int id);

    public EntityUserCreate getUser(Integer id);

}
